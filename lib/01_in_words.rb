# in_words.rb

class Fixnum

  def in_words
    return "zero" if self == 0

    words = []
    string_self = "000000000000000" + self.to_s

    hundreds = string_self[-3..-1].to_i
    thousands = string_self[-6..-4].to_i
    millions = string_self[-9..-7].to_i
    billions = string_self[-12..-10].to_i
    trillions = string_self[-15..-13].to_i

    words << parse_under_100(hundreds)
    words << parse_under_100(thousands) + " thousand" unless thousands == 0
    words << parse_under_100(millions) + " million" unless millions == 0
    words << parse_under_100(billions) + " billion" unless billions == 0
    words << parse_under_100(trillions) + " trillion" unless trillions == 0

    make_readable(words)
  end

  private

  def make_readable(words)
    words.reverse.join(" ").strip
  end

  def parse_under_100(num)
    words = []

    if num % 100 < 20
      words << read_digit(num % 100)
      num /= 100
    else
      words << read_digit(num % 10)
      num /= 10
      words << read_digit_tens(num % 10)
      num /= 10
    end
    words << read_digit(num) + " hundred" if num != 0
    make_readable(words)
  end

  def read_digit(num)
    case num
    when 0
      ""
    when 1
      "one"
    when 2
      "two"
    when 3
      "three"
    when 4
      "four"
    when 5
      "five"
    when 6
      "six"
    when 7
      "seven"
    when 8
      "eight"
    when 9
      "nine"
    when 10
      "ten"
    when 11
      "eleven"
    when 12
      "twelve"
    when 13
      "thirteen"
    when 14
      "fourteen"
    when 15
      "fifteen"
    when 16
      "sixteen"
    when 17
      "seventeen"
    when 18
      "eighteen"
    when 19
      "nineteen"
    end
  end

  def read_digit_tens(num)
    case num
    when 2
      "twenty"
    when 3
      "thirty"
    when 4
      "forty"
    when 5
      "fifty"
    when 8
      "eighty"
    else
      read_digit(num) + "ty"
    end
  end
end
